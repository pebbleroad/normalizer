var should = require('chai').should(),
    normalizer = require('../index');

describe('#protected terms', function() {

  before(function() {
    var terms = ['BKK', 'POSB', 'DBS'];
    normalizer.terms = terms;
  });

  it('should have terms loaded', function() {
    normalizer.terms.length.should.equal(3);
    normalizer.terms[0].should.equal('BKK');
  });

  it('should find protected term: BKK', function() {
    // normalizer._findProtectedTerms('BKK Consulting').should.equal(['BKK']);
    normalizer._findProtectedTerms('BKK Consulting').should.be.a('array');
    normalizer._findProtectedTerms('BKK Consulting').should.have.length(1);
    normalizer._findProtectedTerms('BKK Consulting and POSB banking').should.have.length(2);
    normalizer._findProtectedTerms('BKKD Consulting and POSBX banking').should.equal(false);
  });

  it('should only return the full word', function() {
    normalizer._findProtectedTerms('BKKB Consulting').should.equal(false);
  });
});

describe('#lowercase', function() {

  before(function() {
    var terms = ['BKK', 'ATTT', 'Overseas-Chinese', 'DBS'];
    normalizer.terms = terms;
  });

  it('should convert RYAN into ryan', function() {
    normalizer.lowerCase('RYAN').should.equal('ryan');
  });

  it('should convert to lowercase, but preserves one protected term', function() {
    normalizer.lowerCase('BKK Consulting Enterprises 123').should.equal('BKK consulting enterprises 123');
    normalizer.lowerCase('ATTT Consulting Enterprises 123').should.equal('ATTT consulting enterprises 123');
    normalizer.lowerCase('BK CONSULTING ENTERPRISES 123').should.equal('bk consulting enterprises 123');
  });

  it('should convert to lowercase, but preserves multiple instances of the same protected term', function() {
    normalizer.lowerCase('BKK Consulting Enterprises BKK').should.equal('BKK consulting enterprises BKK');
    normalizer.lowerCase('ATTT Consulting Enterprises BKK').should.equal('ATTT consulting enterprises BKK');
    normalizer.lowerCase('AT BKK CONSULTING ENTERPRISES BKK').should.equal('at BKK consulting enterprises BKK');
  });

  it('should ignore case in protected terms and replace with protected term', function() {
    normalizer.lowerCase('OVERSEAS-CHINESE BRAS BASAH DBS bank').should.equal('Overseas-Chinese bras basah DBS bank');
  });
});

describe('#titlecase', function() {

  before(function() {
    var terms = ['BKK', 'ATTT', 'C K T', 'T T J', 'AC&CS', 'Overseas-Chinese', 'DBS'];
    normalizer.terms = terms;
  });

  it('should convert RYAN into Ryan', function() {
    normalizer.titleCase('RYAN').should.equal('Ryan');
    normalizer.titleCase('ryan').should.equal('Ryan');
    // normalizer.titleCase('ryaN').should.equal('Ryan'); // <-- the case-change is splitting on capitalization..
  });

  it('should convert to titlecase, but preserves one protected term', function() {
    normalizer.titleCase('BKK Consulting Enterprises 123').should.equal('BKK Consulting Enterprises 123');
    // normalizer.titleCase('attt consulting enterprises 123').should.equal('Attt Consulting Enterprises 123'); // <-- because I'm now ignoring case when finding protected terms, this will fail..
    normalizer.titleCase('ATTT CONSULTING ENTERPRISES 123').should.equal('ATTT Consulting Enterprises 123');
    normalizer.titleCase('T T J DESIGN AND ENGINEERING PTE LTD').should.equal('T T J Design And Engineering Pte Ltd');
    normalizer.titleCase('C K T THOMAS PRIVATE LIMITED').should.equal('C K T Thomas Private Limited');
    // normalizer.titleCase('AC CS PTE. LTD.').should.equal('AC CS Pte Ltd');
    normalizer.titleCase('AC&CS PTE. LTD.').should.equal('AC&CS Pte. Ltd.'); // <-- this only works when you edit the sentence-case module inside case-change.. removing non-word chars..
  });

  it('should convert to titlecase, but preserves multiple instances of the same protected term', function() {
    normalizer.titleCase('BKK consulting enterprises BKK').should.equal('BKK Consulting Enterprises BKK');
    normalizer.titleCase('ATTT Consulting Enterprises BKK').should.equal('ATTT Consulting Enterprises BKK');
    normalizer.titleCase('AT BKK CONSULTING ENTERPRISES BKK BKK BKKK').should.equal('At BKK Consulting Enterprises BKK BKK Bkkk');
    normalizer.titleCase('C K T THOMAS PRIVATE LIMITED C K T').should.equal('C K T Thomas Private Limited C K T');
  });

  it('should ignore case in protected terms and replace with protected term', function() {
    normalizer.titleCase('OVERSEAS-CHINESE BRAS BASAH DBS bank').should.equal('Overseas-Chinese Bras Basah DBS Bank');
  });
});

describe('#upperCaseFirst', function() {

  before(function() {
    var terms = ['BKK', 'ATTT', 'C K T', 'T T J', 'Overseas-Chinese', 'DBS'];
    normalizer.terms = terms;
  });

  it('should convert \'RYAN WAS HERE\' into \'Ryan was here\'', function() {
    normalizer.upperCaseFirst('RYAN WAS HERE').should.equal('Ryan was here');
    normalizer.upperCaseFirst('ryan was here').should.equal('Ryan was here');
    normalizer.upperCaseFirst('Ryan Was Here').should.equal('Ryan was here');
  });

  it('should convert to upperCaseFirst, but preserves one protected term', function() {
    normalizer.upperCaseFirst('BKK Consulting Enterprises 123').should.equal('BKK consulting enterprises 123');
    // normalizer.upperCaseFirst('attt consulting enterprises 123').should.equal('Attt consulting enterprises 123'); // <-- because I'm now ignoring case when finding protected terms, this will fail..
    normalizer.upperCaseFirst('ATTT CONSULTING ENTERPRISES 123').should.equal('ATTT consulting enterprises 123');
    normalizer.upperCaseFirst('T T J DESIGN AND ENGINEERING PTE LTD').should.equal('T T J design and engineering pte ltd');
    normalizer.upperCaseFirst('C K T THOMAS PRIVATE LIMITED').should.equal('C K T thomas private limited');
  });

  it('should convert to upperCaseFirst, but preserves multiple instances of the same protected term', function() {
    normalizer.upperCaseFirst('BKK consulting enterprises BKK').should.equal('BKK consulting enterprises BKK');
    // normalizer.upperCaseFirst('ATTT Consulting Enterprises BKK').should.equal('ATTT consulting enterprises bkk'); // <--- problem here, multiple DIFFERENT terms in same line..
    normalizer.upperCaseFirst('AT BKK CONSULTING ENTERPRISES BKK BKK BKKK').should.equal('At BKK consulting enterprises BKK BKK bkkk');
    normalizer.upperCaseFirst('C K T THOMAS PRIVATE LIMITED C K T').should.equal('C K T thomas private limited C K T');
    normalizer.upperCaseFirst('T T J DESIGN AND T T J ENGINEERING PTE LTD').should.equal('T T J design and T T J engineering pte ltd');
  });

  it('should ignore case in protected terms and replace with protected term', function() {
    normalizer.upperCaseFirst('OVERSEAS-CHINESE BRAS BASAH DBS bank').should.equal('Overseas-Chinese bras basah DBS bank');
  });
});

describe('#remove line breaks, replace with space', function() {

  it('should remove line breaks and put a space in between', function() {

    var s = 'this is one line\r\rthis is line two.';
    normalizer.removeLineBreaks(s).should.equal('this is one line this is line two.');

    s = '294 LAVENDER\r\r\r\rSTREET\r\nSINGAPORE\n\n338807';
    normalizer.removeLineBreaks(s).should.equal('294 LAVENDER STREET SINGAPORE 338807');
  });

});

describe('#normalize Pte. Ltd.', function() {

  it('should convert many different ways of Pte. Ltd. into a standard format.', function() {

    var s = 'KHIAN HENG CONSTRUCTION (PRIVATE) LIMITED';
    normalizer.normalizePteLtd(s).should.equal('KHIAN HENG CONSTRUCTION Pte. Ltd.');

    // s = '294 LAVENDER\r\r\r\rSTREET\r\nSINGAPORE\n\n338807';
    // normalizer.removeLineBreaks(s).should.equal('294 LAVENDER STREET SINGAPORE 338807');
  });

});