var caseChange = require('./libs/change-case'),
    _ = require('lodash'),
    lineReader = require('line-reader');

module.exports = {

  terms: [],

  /**
   * load terms from text file..
   */
  loadTerms: function (file, cb) {
    var self = this;

    lineReader.eachLine(file, function (line, last) {
      self.terms.push (line);
    }).then(function() {
      // console.log (self.terms);
      cb();
    });
  },

  /**
   * lowercases terms but protects terms found in array..
   */
  lowerCase: function (str) {
    var pWords = this._findProtectedTerms(str),
        transformed = caseChange.lowerCase(str);

    // replace original words..
    transformed = this._restoreProtectedWords(pWords, transformed);

    return transformed;
  },

  /**
   * titlecases terms but protects terms found in array..
   */
  titleCase: function (str) {
    var pWords = this._findProtectedTerms(str),
        transformed = caseChange.titleCase(str);

    // replace original words..
    transformed = this._restoreProtectedWords(pWords, transformed);

    return transformed;
  },

  /**
   * upperCaseFirst terms but protects terms found in array..
   */
  upperCaseFirst: function (str) {
    var pWords = this._findProtectedTerms(str),
        transformed = caseChange.lowerCase(str);

    transformed = caseChange.upperCaseFirst(transformed);

    // replace original words..
    transformed = this._restoreProtectedWords(pWords, transformed);

    return transformed;
  },

  /**
   * removeLineBreaks removes enters and line breaks,
   * replacing them with a space..
   */
  removeLineBreaks: function (str) {
    return str.replace(/(\n|\r)+/gm, ' ');
  },

  /**
   * normalizePteLtd converts many different ways that
   * Pte. Ltd. is written into a standard format..
   */
  normalizePteLtd: function (str) {
    // var regEx = new RegExp(/\(?(pte(\.?)|private)\)?/, 'gi');
    var str = str.replace(/\(?(pte|private)\.?\)?/gi, 'Pte.');
    str = str.replace(/\(?(ltd|limited)\.?\)?/gi, 'Ltd.');
    return str;
  },

  _findProtectedTerms: function (str) {
    var regEx,
        res = _.filter(this.terms, function (t) {
          regEx = new RegExp('\\b' + t + '\\b', 'i');
          return regEx.test(str);
        });

    if (res.length === 0) {
      return false;
    }
    else {
      return res;
    }
  },

  _restoreProtectedWords: function (pWords, term) {
    if (pWords) {
      _.each(pWords, function (pWord) {
        var reg = new RegExp('\\b' + pWord + '\\b', 'gi');
        term = term.replace(reg, pWord);
      });
    }
    return term;
  }
};