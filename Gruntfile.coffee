module.exports = (grunt) ->

  # Tasks
  grunt.initConfig {

    # ----------------------
    # Clean
    # ----------------------
    # clean:
    #   src: ['!public/js/templates.js', 'public/js']
    #   templates: ['public/js/templates.js']

    # ----------------------
    # Mocha tests..
    # ----------------------
    mochaTest:
      test:
        options:
          reporter: 'spec'
          quiet: false
          clearRequireCache: false
        src: ['test/**/*.js']

    # ----------------------
    # Watch
    # ----------------------
    # watch:
    #   scripts:
    #     files: 'public/coffee/**/*.coffee'
    #     tasks: ['coffee']
    #   handlebars:
    #     files: 'public/templates/**/*.hbs'
    #     tasks: ['handlebars']

  }

  # Plugins
  require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks)

  # Commands
  grunt.registerTask 'default', ['']
  grunt.registerTask 'test', ['mochaTest']

  return grunt