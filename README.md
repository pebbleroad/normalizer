MOM DBapp Normalizer
====================

An npm module to normalize text fields for MOM DBapp. Capable of the following

* Changing the case of fields with an optional text file with terms to protect from case changing. This is useful for protecting acronyms and such.
* Normalizing "Pte. Ltd."
* Removing line breaks from words.

Usage
-----


